@extends('layouts.master')
@section('title')
    Halaman Daftar
@endsection
@section('content')
<h2>Halaman Biodata</h2>
<form action="/welcome" method="POST">
    @csrf
    <label>Nama lengkap</label><br>
    <input type="text" name="fnama"><br><br>
    <label>usia</label><br>
    <input type="number" nama="usia"><br><br>
    <label>Password</label><br>
    <input type="password" nama="pass"><br><br>
    <label>Gender</label><br>
    <input type="radio" name="Gender" value="1">laki-laki<br>
    <input type="radio" name="Gender" value="2">Perempuan<br><br>
    <label>Nationality</label><br>
    <select name="national"><br>
        <option value="indonesi">Indonesia</option>
        <option value="malaysia">malaysia</option>
        <option value="singapure">singapure</option>
        <option value="india">india</option>
        <option value="Arab">arab</option>
    </select><br><br>
    <label>language spoken</label><br>
    <input type="checkbox" name="bahasa">Indonesia<br>
    <input type="checkbox" name="bahasa">English<br>
    <input type="checkbox" name="bahasa">other<br>
    <label>Bio</label><br>
    <textarea name="message" rows="10" cols="30"></textarea><br>
    <input type="submit" value="Kirim">
</form>
@endsection