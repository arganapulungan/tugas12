@extends('layouts.master')
@section('title')
    Data Pemain 
@endsection
@section('content')

<table class="table">
  <thead>
    <tr>
      <th scope="col">id</th>
      <th scope="col">Name</th>
      <th scope="col">umur</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
        @forelse ($cast as $key => $item)
        <tr>
            <th scope="row">{{$key +1 }}</th>
            <td>{{$item->name}}</td>
            <td>{{$item->umur}}</td>
            <td>
                <form action="/cast/{{$item->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/cast/{{$item->id}}" class="btn btn-sm btn-info">Detail</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-sm btn-warning">edit</a>
                    <input type="submit" value="delete" class="btn btn-sm btn-danger">

                </form>
            </td>
        </tr>
        @empty
        <p>tidak ada data pemain</p>
        @endforelse
  </tbody>
</table>



@endsection