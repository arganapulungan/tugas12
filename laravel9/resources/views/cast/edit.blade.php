@extends('layouts.master')
@section('title')
    Daftar Data Pemain Baru
@endsection
@section('content')

@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action = "/cast/{{$cast->id}}}" method="POST">
    @csrf
    @method('PUT')
  <div class="form-group">
    <label>Name</label>
    <input type="text" name="name" value="{{$cast->name}}" class="form-control">
  </div>
  <div class="form-group">
    <label>umur</label>
    <input type="number" name="umur" value="{{$cast->umur}}"class="form-control">
  </div>
  <div class="form-group">
    <label>Bio:</label>
    <textarea name="bio" class="form-control" id="" cols="30" rows="10">{{$cast->bio}}</textarea>
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection