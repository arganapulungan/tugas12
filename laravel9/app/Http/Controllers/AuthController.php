<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('page.register');
    }

    public function welcome(Request $request){

        $namaLengkap = $request-> input('fname');


        return view('page.welcome',["namaLengkap" => $namaLengkap]);
    }
}
